{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ConstrainedClassMethods #-}

-- | Simplicial complexes and homotopy types.
module Surfacer.Complex
    ( -- * Connectivity
      Connectedness(Infinite, Only)
    , Connective
    , ConnectiveBox(ConnectiveBox)
    , ConnectiveComplexBox(ConnectiveComplexBox)
    , Contractible(Contractible)
      -- * Simplicial complexes
    , Complex
    , conn
    , dim
    , isEmpty
    , prop_Emptiness
      -- * Basic constructions of simplicial complexes
    , Empty(Empty)
    , Sphere(Sphere)
    , Join
    , JoinLs
    , (∗)
    , Suspension
    )
where

import           Data.List
import           Test.QuickCheck

-- | Connectedness of a homotopy type.
--
-- \((-2)\)-connected is vacuous. \((-1)\)-connected means nonempty.
data Connectedness = Infinite
                  -- ^ Contractible.
                   | Only' Int
                  -- ^ \((n)\)-connected.
    deriving Eq
pattern Only
    :: Int
    -- ^ Integer \(n\)
    -> Connectedness
    -- ^ Representing \(n\)-connectedness (normalized to \(n = -2\)
    --   if \(n < -2\))
pattern Only n <- Only' n where
    Only n = Only' (max (-2) n)

instance Show Connectedness where
    show Infinite = "\\infty"
    show (Only x) | x < 0     = "(" ++ show x ++ ")"
                  | otherwise = show x
instance Ord Connectedness where
    compare Infinite Infinite = EQ
    compare Infinite _        = GT
    compare _        Infinite = LT
    compare (Only x) (Only y) = compare x y
-- | Some operations are nonsensical to perform on connectedness and these will
--   throw exceptions.
instance Num Connectedness where
    Infinite + _        = Infinite
    _        + Infinite = Infinite
    Only x   + Only y   = Only (x + y)
    _        - Infinite = error "subtracting infinity"
    Infinite - _        = Infinite
    Only x   - Only y   = Only (x - y)
    _        * Infinite = Infinite
    Infinite * _        = Infinite
    Only x   * Only y   = Only (x * y)
    negate Infinite = undefined
    negate (Only _) = undefined
    abs             = undefined
    signum          = undefined
    fromInteger x = Only $ fromInteger x

-- | A simplicial complex.
class Complex a where
    -- | The dimension of the simplicial complex.
    dim :: a
        -- ^ Simplicial complex \(X\)
        -> Int
        -- ^ Dimension \(\geq -1\) of \(X\) (\(-1\) if \(X = \emptyset\))

    -- | Is the simplicial complex empty?
    --
    -- This is determined from 'dim' and should not be overwritten.
    isEmpty :: a -> Bool
    isEmpty a = dim a < 0
-- | A homotopy type for which a connectivity estimate is known.
class Connective a where
    -- | A lower bound for the connectedness of a homotopy type.
    conn :: a -> Connectedness
    -- | Tests if the estimate is indeed \(-2\) if the simplicial complex is
    --   empty.
    prop_Emptiness :: Complex a => a -> Property
    prop_Emptiness c =
        isEmpty c ==> conn c == Only (-2)

-- | Dynamic wrapper for a homotopy type with connectivity estimate.
--
-- This is useful if you are mixing many different complexes and you want to
-- escape from typechecking hell.
data ConnectiveBox = forall c. (Connective c, Show c) => ConnectiveBox c
instance Show ConnectiveBox where
    show (ConnectiveBox c) = show c
instance Connective ConnectiveBox where
    conn (ConnectiveBox c) = conn c
-- | Dynamic wrapper for a simplicial complex with connectivity estimate.
--
-- This is useful if you are mixing many different complexes and you want to
-- escape from typechecking hell.
data ConnectiveComplexBox = forall c. (Connective c, Complex c, Show c) => ConnectiveComplexBox c
instance Show ConnectiveComplexBox where
    show (ConnectiveComplexBox c) = show c
instance Complex ConnectiveComplexBox where
    dim (ConnectiveComplexBox c) = dim c
instance Connective ConnectiveComplexBox where
    conn (ConnectiveComplexBox c) = conn c

-- | The contractible homotopy type \((*)\).
data Contractible = Contractible deriving Eq
instance Show Contractible where
    show Contractible = "(*)"
instance Connective Contractible where
    conn _ = Infinite

-- | The empty simplicial complex.
data Empty = Empty deriving Eq
instance Show Empty where
    show Empty = "\\emptyset"
instance Complex Empty where
    dim _ = -1
instance Connective Empty where
    conn _ = Only (-2)

-- | A simplicial sphere.
--
-- This could represent any triangulation of \(S^n\). For instance,
-- \(\partial \Delta^{n + 1}\).
--
-- We use the convention \(S^{-1} = \emptyset\).
newtype Sphere = Sphere' Int deriving Eq
pattern Sphere
    :: Int
    -- ^ Dimension \(n\) (throws exception if \(n < -1\))
    -> Sphere
    -- ^ Sphere \(S^n\)
pattern Sphere n <- Sphere' n where
    Sphere n | n < -1    = error "creating sphere of dimension < -1"
             | otherwise = Sphere' n
instance Show Sphere where
    show (Sphere n) | n == -1          = show Empty
                    | n >= 0 && n <= 9 = "S^" ++ show n
                    | otherwise        = "S^{" ++ show n ++ "}"
instance Complex Sphere where
    dim (Sphere n) = n
instance Connective Sphere where
    conn (Sphere n) = Only (n - 1)

-- | The join of two simplicial complexes or, more generally, two homotopy
--   types.
data Join a b = Join a b deriving Eq
instance (Show a, Show b) => Show (Join a b) where
    show (Join a b) = show a ++ " * " ++ show b
instance (Complex a, Complex b) => Complex (Join a b) where
    dim (Join a b) = dim a + 1 + dim b + 1 - 1
instance (Connective a, Connective b) => Connective (Join a b) where
    conn (Join a b) = conn a + 2 + conn b + 2 - 2
-- | The join of two simplicial complexes.
(∗) :: a -> b -> Join a b
(∗) = Join

-- | The join of a list of simplicial complexes or, more generally, homotopy
--   types.
newtype JoinLs a = JoinLs [a] deriving Eq
instance Show a => Show (JoinLs a) where
    show (JoinLs a) = intercalate " * " $ map show a
instance Complex a => Complex (JoinLs a) where
    dim (JoinLs a) = sum (map (\x -> 1 + dim x) a) - 1
instance Connective a => Connective (JoinLs a) where
    conn (JoinLs a) = sum (map (\x -> Only 2 + conn x) a) - Only 2

-- | The suspension of a simplicial complex or, more generally, a homotopy
--   type.
newtype Suspension a = Suspension a deriving Eq
instance Show a => Show (Suspension a) where
    show (Suspension a) = "S " ++ show a
instance (Complex a) => Complex (Suspension a) where
    dim (Suspension a) = dim a + 1
instance (Connective a) => Connective (Suspension a) where
    conn (Suspension a) = conn a + 1
