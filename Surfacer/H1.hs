{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE LambdaCase #-}

module Surfacer.H1 where

import Prelude hiding ((++))
import Data.List hiding ((++))
import Data.Maybe
import Data.Bits
import Surfacer.Surface hiding ((⊕))
import qualified Surfacer.Surface as Surface
import qualified Surfacer.Permutation          as Permutation
import           Surfacer.Permutation           ( LabeledPermutation )
import Surfacer.AbelianGroup
import Surfacer.ArcComplex
import Surfacer.OrientedSystem
import Surfacer.Orbits
import Test.QuickCheck hiding (scale)
import Control.Arrow

(++) :: Monoid m => m -> m -> m
(++) = mappend
(∈) :: Eq a => a -> [a] -> Bool
(∈) = elem
(∉) :: Eq a => a -> [a] -> Bool
(∉) = notElem
infix 0 ∈
infix 0 ∉

-- | An identifier for a boundary component of a surface.
type BoundaryId = Int

-- | A conjugacy class of an element of the mapping class group of a surface.
--
-- We assume that every boundary component on the ambient surface is labeled by
-- @0, 1, ... :: BoundaryId@.
data MCGElement = TwoSidedNonseparatingTwistOrientableComplement
                -- ^ A Dehn twist along a nonseparating, two-sided curve with
                --   orientable complement, in direction of the orientation if on
                --   an oriented surface.
                | TwoSidedNonseparatingTwistNonorientableComplement
                -- ^ A Dehn twist along a nonseparating, two-sided curve with
                --   non-orientable complement.
                | BoundaryTwist BoundaryId
                -- ^ A Dehn twist along a curve winding around a boundary component.
                | DoubleBoundaryTwist' BoundaryId BoundaryId
                -- ^ A Dehn twist along a curve winding around two boundaries,
                --   cutting off a genus 0 connected component.
                | CrosscapTransposition
                -- ^ A half Dehn twist swapping one crosscap by another.
                deriving (Eq, Ord)
pattern DoubleBoundaryTwist :: BoundaryId -> BoundaryId -> MCGElement
pattern DoubleBoundaryTwist b1 b2 <- DoubleBoundaryTwist' b1 b2 where
    DoubleBoundaryTwist b1 b2 | b1 < b2 = DoubleBoundaryTwist' b1 b2
                              | b1 > b2 = DoubleBoundaryTwist' b2 b1

instance Show MCGElement where
    show TwoSidedNonseparatingTwistNonorientableComplement = "a"
    show TwoSidedNonseparatingTwistOrientableComplement    = "b"
    show (BoundaryTwist n)                                 = "s_" ++ latexNumber n
    show (DoubleBoundaryTwist n m)                         = "s_{" ++ show n ++ ", " ++ show m ++ "}"

-- | Returns a representation of the group \(H_1(\Gamma(S))\) for a surface
--   \(S\).
mcgAbelianization :: Surface -> AbGrp MCGElement
-- We do not handle closed surfaces.
mcgAbelianization (S g 0) = undefined
mcgAbelianization (N g 0) = undefined
-- We don't refer to the last boundary, because as it turns out, any element
-- involving it can be rewritten as a linear combination of elements not
-- refering to it.
mcgAbelianization (S 0 r) =
      mconcat [ ℤ ⇜ BoundaryTwist b
              | b <- [0 .. r - 2]
              ]
    ⊕ mconcat [ ℤ ⇜ DoubleBoundaryTwist b1 b2
              | b1 <- [0 .. r - 3]
              , b2 <- [b1 + 1 .. r - 2]
              ]
mcgAbelianization (S 1 r) =
    ℤ ⇜ TwoSidedNonseparatingTwistOrientableComplement
    ⊕ mconcat [ ℤ ⇜ BoundaryTwist b
              | b <- [0 .. r - 2]
              ]
mcgAbelianization (S 2 r) = Z 12 ⇜ TwoSidedNonseparatingTwistOrientableComplement
mcgAbelianization (S g r) = trivial
-- TODO: I am unsure as to whether presentations are known in the case of
--       low genus and multiple boundary components. Perhaps one could
--       bootstrap abelianizations from presentations involving punctures and
--       the SES relating boundary components to punctures.
-- The groups that follow can be computed by abelianizing the infinite
-- presentations provided by Kobayashi--Omori.
mcgAbelianization (N 1 r) = mconcat [ Z 2 ⇜ BoundaryTwist b
                                    | b <- [0 .. r - 2]
                                    ]
mcgAbelianization (N 2 r) = Z 2 ⇜ TwoSidedNonseparatingTwistOrientableComplement
                          ⊕ ℤ   ⇜ CrosscapTransposition
                          ⊕ mconcat [ Z 2 ⇜ BoundaryTwist b
                                    | b <- [0 .. r - 2]
                                    ]
mcgAbelianization (N 3 r) = Z 2 ⇜ TwoSidedNonseparatingTwistNonorientableComplement
                          ⊕ Z 2 ⇜ CrosscapTransposition
                          ⊕ mconcat [ Z 2 ⇜ BoundaryTwist b
                                    | b <- [0 .. r - 2]
                                    ]
mcgAbelianization (N 4 r) = Z 2 ⇜ TwoSidedNonseparatingTwistNonorientableComplement
                          ⊕ Z 2 ⇜ TwoSidedNonseparatingTwistOrientableComplement
                          ⊕ Z 2 ⇜ CrosscapTransposition
                          ⊕ mconcat [ Z 2 ⇜ BoundaryTwist b
                                    | b <- [0 .. r - 2]
                                    ]
mcgAbelianization (N g r)
    | g ∈ [5, 6]
    = Z 2 ⇜ TwoSidedNonseparatingTwistNonorientableComplement
    ⊕ Z 2 ⇜ CrosscapTransposition
mcgAbelianization (N g r)
    | g >= 7
    = Z 2 ⇜ CrosscapTransposition

-- | A linear combination of conjugacy classes of mapping classes, representing
--   an element in the abelianization of the mapping class group of a surface.
type H1Γ = LinearCombination MCGElement

-- | Relabels the boundary identifiers.
relabelBoundary :: (BoundaryId -> BoundaryId) -> H1Γ -> H1Γ
relabelBoundary f x = fmap
    (\case
        TwoSidedNonseparatingTwistOrientableComplement    -> TwoSidedNonseparatingTwistOrientableComplement
        TwoSidedNonseparatingTwistNonorientableComplement -> TwoSidedNonseparatingTwistNonorientableComplement
        BoundaryTwist b                                   -> BoundaryTwist (f b)
        DoubleBoundaryTwist a b                           -> DoubleBoundaryTwist (f a) (f b)
        CrosscapTransposition                             -> CrosscapTransposition
    )
    x

-- | The abelianized image of the mapping class, expressed in terms of the
--   generating set of 'mcgAbelianization', of a Dehn twist encircling a genus 0
--   subsurface containing a subset of the boundary components.
boundaryTwist :: Surface -> [BoundaryId] -> H1Γ
boundaryTwist surface ls
    | nub (sort ls) /= sort ls
    = error "specifying the same boundary component multiple times"
    | not $ nub (sort ls) `isSubsequenceOf` [0 .. r surface - 1]
    = error "specifying boundary component that surface does not have"
boundaryTwist _ [] = zero
boundaryTwist (S _ 0) _  = error "no boundaries to twist"
boundaryTwist (S 0 r) boundaries
    -- If the last boundary component is among the boundaries to twist around
    -- and the genus is zero, we can equivalently twist along the complement of
    -- the subset of components.
    | r - 1 ∈ boundaries
    = boundaryTwist (S 0 r)
                    [ b
                    | b <- [0 .. r - 1]
                    , b ∉ boundaries
                    ]
boundaryTwist (S 0 r) [b]      = 1 ⋅ BoundaryTwist b
boundaryTwist (S 0 r) [b1, b2] = 1 ⋅ DoubleBoundaryTwist b1 b2
boundaryTwist (S 0 r) (b1:b2:xs) =
    -- This is valid by the lantern relation.
       boundaryTwist (S 0 r) [b1, b2]
    ++ boundaryTwist (S 0 r) (b1:xs)
    ++ boundaryTwist (S 0 r) (b2:xs)
    ++ (minus $ boundaryTwist (S 0 r) xs)
    ++ (minus $ boundaryTwist (S 0 r) [b1])
    ++ (minus $ boundaryTwist (S 0 r) [b2])
-- This is valid by the degenerate star relation. See Farb--Margalit.
boundaryTwist (S 1 1) [0] = 12 ⋅ TwoSidedNonseparatingTwistOrientableComplement
boundaryTwist (S 1 r) [i] | i < r - 1 = 1 ⋅ BoundaryTwist i
boundaryTwist (S 1 r) boundaries
    | r - 1 ∈ boundaries
    -- This is valid by the degenerate star relation. See Farb--Margalit.
    = (12 ⋅ TwoSidedNonseparatingTwistOrientableComplement)
    ++ (minus $ boundaryTwist
        (S 1 r)
        [ b
        | b <- [0 .. r - 1]
        , b ∉ boundaries
        ])
    -- We compute the twist by a little trick. First, we glue a disk onto the
    -- 0'th boundary component. On the abelianization of the mapping class
    -- group, doing so has the effect of killing the generator
    -- @BoundaryTwist 0@. Recursively, we can compute the twist under this map.
    -- This determines the coefficients to all generators except for
    -- @BoundaryTwist 0@. We determine this coefficient by instead gluing a
    -- disk to the 1'st component.
    | r >= 3
    = relabelBoundary
          (\k -> k + 1)
          (boundaryTwist (S 1 (r - 1)) (map (lower 0) (delete 0 boundaries)))
    ++ (project
          (BoundaryTwist 0)
          (boundaryTwist (S 1 (r - 1)) (map (lower 1) (delete 1 boundaries)))
          ⋅ BoundaryTwist 0)
-- This is valid because gluing disks onto each boundary component induces an
-- isomorphism on abelianizations of mapping class groups of genus 2 surfaces
-- and this isomorphism kills this element, thus witnessing its triviality.
boundaryTwist (S g r) _ = zero
boundaryTwist (N g 0) _ = error "no boundaries to twist"
-- Trivial because it is an element in a trivial group.
boundaryTwist (N 1 1) _ = zero
-- TODO: Figure out this computation. Stukow is insufficient, it appears. Also,
--       we only even know the generating set in the case @r = 1@.
boundaryTwist (N 2 r) boundaries = undefined
boundaryTwist (N g r) [b]
    | g ∈ [3, 4]
    -- Stukow, "Generating mapping class groups of nonorientable surfaces with
    -- boundary", Lemma 6.15.
    = if b == r - 1 then
          minus $ mconcat [boundaryTwist (N g r) [b'] | b' <- [0 .. r - 2]]
      else
          1 ⋅ BoundaryTwist b
boundaryTwist (N g r) [b]
    | g >= 5
    -- Stukow, Lemma 6.12.
    = zero
boundaryTwist (N g r) boundaries
    | g >= 3
    -- Stukow Lemma 6.14.
    = mconcat [boundaryTwist (N g r) [b] | b <- boundaries]

-- | Tests that the in the result of 'boundaryTwist' are in fact among the
--   chosen generators for the abelianization of the corresponding mapping class
--   group.
prop_BoundaryTwistInGroup surface = do
    boundaries <- sublistOf [0..r surface - 1]
    let gen  = generators (mcgAbelianization surface)
    let gen' = map snd (terms (boundaryTwist surface boundaries))
    return $ all (∈ gen) gen'
-- | Tests that 'boundaryTwist' commutes with relabeling.
prop_BoundaryTwistRelabelCommute surface = do
    boundaries <- sublistOf [0..r surface - 1]
    shuffledLabels <- shuffle [0..r surface - 1]
    return $ equalIn
        (mcgAbelianization surface)
        (boundaryTwist surface (map (shuffledLabels !!) boundaries))
        (relabelBoundary (shuffledLabels !!) (boundaryTwist surface boundaries))

-- | Computes an element of \(H_1(Γ(S))\) given a linear combination of
--   conjugacy classes of mapping classes.
computeH1ΓElement :: Surface -> LinearCombination MCGElement -> H1Γ
computeH1ΓElement (S _ 0) = undefined
computeH1ΓElement (N _ 0) = undefined
computeH1ΓElement surface = (>>=
    \case
        BoundaryTwist b -> boundaryTwist surface [b]
        DoubleBoundaryTwist b1 b2 -> boundaryTwist surface [b1, b2]
        CrosscapTransposition -> 1 ⋅ CrosscapTransposition
        TwoSidedNonseparatingTwistOrientableComplement ->
            case surface of
                -- The group is trivial.
                S g _ | g >= 3 -> zero
                S _ _ -> 1 ⋅ TwoSidedNonseparatingTwistOrientableComplement
                -- Stukow, Lemma 6.6.
                N g _ | g >= 6 -> zero
                N _ _ -> 1 ⋅ TwoSidedNonseparatingTwistOrientableComplement
        TwoSidedNonseparatingTwistNonorientableComplement ->
            case surface of
                -- Kobayashi--Omori relation (A6).
                N g _ | g >= 7 -> zero
                N _ _ -> 1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
    )

-- | The abelianized image of the mapping class, expressed in terms of the
--   generating set of 'mcgAbelianization', of a Dehn twist encircling a
--   subsurface with a single boundary component and a subset of the boundary
--   components.
boundaryTwistPlus :: Surface -> Surface -> [BoundaryId] -> H1Γ
-- We reduce this to 'boundaryTwist' by cutting out the subsurface introducing
-- a new boundary component, labeled with the highest label.
boundaryTwistPlus surface subsurface b
    | r subsurface /= 1
    = error "the additional subsurface to twist around cannot have multiple connected components"
    | otherwise
    = computeH1ΓElement surface $ (
      boundaryTwist holed ((r holed - 1):b) >>= \case
        TwoSidedNonseparatingTwistOrientableComplement ->
            1 ⋅ if isOrientable subsurface then TwoSidedNonseparatingTwistOrientableComplement
                                           else TwoSidedNonseparatingTwistNonorientableComplement
        TwoSidedNonseparatingTwistNonorientableComplement ->
            1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
        CrosscapTransposition ->
            1 ⋅ CrosscapTransposition
        -- Note that @i@ is never the last boundary component by choice of
        -- generating set.
        BoundaryTwist i | i /= r holed - 1 ->
            boundaryTwist surface [i]
        -- Note that @i, j@ are never the last boundary component, for the same
        -- reason.
        DoubleBoundaryTwist i j | r holed - 1 ∉ [i, j] ->
            boundaryTwist surface [i, j]
    )
  where
    holed = case (surface, subsurface) of
                (N g r, N g' r') -> N (g - g') (r - r')
                (N g r, S g' r') -> N (g - 2 * g') (r - r')
                (S g r, S g' r') -> S (g - g') (r - r')
-- | Tests that 'boundaryTwistPlus' with respect to \(S_{0, 1}\) is just
--   'boundaryTwist'.
prop_BoundaryTwistPlusDisk surface = do
    boundaries <- sublistOf [0..r surface - 1]
    return $ equalIn
        (mcgAbelianization surface)
        (boundaryTwist surface boundaries)
        (boundaryTwistPlus surface (S 0 1) boundaries)

-- | Computes the homomorphism \(H_1(\Gamma(S)) \to H_1(\Gamma(S \oplus S'))\)
--   extending mapping classes from a summand to a boundary sum by mapping the
--   glued piece identically.
boundarySumExtension
    :: Surface
    -- ^ Surface to start with
    -> Surface
    -- ^ Surface to boundary sum onto
    -> BoundaryId
    -- ^ Boundary component along which the boundary sum is done
    -> Homomorphism MCGElement MCGElement
    -- ^ \(H_1(\Gamma(S)) \to H_1(\Gamma(S \oplus S'))\)
-- TODO: Because of how 'boundaryTwistPlus' is limited, this currently only
--       supports the case where @r summand == 1@.
boundarySumExtension surface summand b = mkHomomorphism
    (mcgAbelianization surface)
    (mcgAbelianization sum) $
    computeH1ΓElement sum .
    \case
        TwoSidedNonseparatingTwistNonorientableComplement ->
            1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
        CrosscapTransposition ->
            1 ⋅ CrosscapTransposition
        TwoSidedNonseparatingTwistOrientableComplement ->
            1 ⋅ if isOrientable summand then TwoSidedNonseparatingTwistOrientableComplement
                                        else TwoSidedNonseparatingTwistNonorientableComplement
        BoundaryTwist b' | b == b' -> boundaryTwistPlus sum summand [b]
        BoundaryTwist b' -> 1 ⋅ BoundaryTwist b'
        DoubleBoundaryTwist b1 b2 | b ∈ [b1, b2] ->
            boundaryTwistPlus sum summand [b1, b2]
        DoubleBoundaryTwist b1 b2 -> 1 ⋅ DoubleBoundaryTwist b1 b2
  where sum = ((Surface.⊕) surface summand)
-- | Tests that 'boundarySumExtension' respects the associativity of boundary
--   sums.
prop_BoundarySumExtensionAssoc surface = do
    b <- elements [0..r surface]
    summand1 <- arbitrary `suchThat` ((== 1) . r)
    summand2 <- arbitrary `suchThat` ((== 1) . r)
    let sum = ((Surface.⊕) summand1 summand2)
    return $
           boundarySumExtension surface sum b
        == (boundarySumExtension ((Surface.⊕) surface summand1) summand2 b)
         ∘ (boundarySumExtension surface summand1 b)

-- | Basepoints used to encode systems of arcs.
--
-- We consider systems of arcs between two points \(b_0\) and \(b_1\), although
-- we are forced to represent these by means of multiple basepoints as each
-- basepoint can be used by at most one arc.
data Basepoint = Endpoint1 Int
               -- ^ Basepoint corresponding to \(b_0\). We label these
               --   basepoints from 0 and upwards moving clockwise around the
               --   boundary.
               | Endpoint2 Int
               -- ^ Basepoint corresponding to \(b_1\). We label these
               --   basepoints from 0 and upwards moving clockwise around the
               --   boundary.
               | Marker1Before Int
               -- ^ Marker basepoint @Marker1Before i@ placed clockwisely
               --   before the basepoint @Endpoint1 i@.
               | Marker1After Int
               -- ^ Marker basepoint @Marker1After i@ placed clockwisely
               --   after the basepoint @Endpoint1 i@ if @i@ is nonnegative,
               --   or marker basepoint for unused boundary if @i@ is negative.
               | Marker2Before Int
               -- ^ Marker basepoint @Marker2Before i@ placed clockwisely
               --   before the basepoint @Endpoint2 i@.
               | Marker2After Int
               -- ^ Marker basepoint @Marker2After i@ placed clockwisely
               --   before the basepoint @Endpoint2 i@.
               deriving (Eq, Ord)

-- | Intertwines two lists, starting with the first element of the first list
--   and then alternating between elements of the two lists.
intertwine :: [a] -> [a] -> [a]
intertwine []     ys = ys
intertwine (x:xs) ys = x:intertwine ys xs

-- | Creates a system of arcs with basepoints marked with 'Basepoint' (and
--   conforming to its documentation) from a permutation.
--
-- No matter how many arcs are cut, every boundary will admit a basepoint of
-- kind @Marker1After i@. Such a marker with the smallest @i@ is called the
-- representative marker. Each boundary in a surface with any number of arcs
-- cut is given a 'BoundaryId' which is an integer ranging from 0 up to
-- @r - 1@ for @r@ the number of boundaries, assigned according to the order of
-- the representative markers.
twoBasepointsMarked
    :: Surface
    -- ^ Surface on which the arcs live
    -> BoundaryConfiguration
    -- ^ Boundary configuration (throws exception if there are not enough
    --   boundary components)
    -> LabeledPermutation Orientation
    -- ^ Permutation of the basepoints labeled by what orientation should slide
    --   to 'Anticlockwise' along the corresponding arc
    -> OrientedSystem Basepoint
    -- ^ Resulting system of arcs
twoBasepointsMarked surface config perm
    | r surface == 0 || config == Different && r surface < 2
    = error "not enough boundary components for configuration to be sensible"
    | otherwise
    = mkOrientedSystem
        surface
        (case config of
            Different ->
                [ BoundaryCircle
                    (concat
                        -- TODO: Idea: store orientations at markers
                        -- TODO: Question: How do we determine twist based on just one arc?
                        [ [ (Marker1Before i, undefined)
                          , (Endpoint1 i, perm `Permutation.labelAt` i)
                          , (Marker1After i, undefined)
                          ]
                        | i <- Permutation.domain perm
                        ]
                    )
                , BoundaryCircle
                    (concat
                        -- We use @undefined@ for the orientation since it
                        -- should never be accessed.
                        [ [ (Marker2Before i, undefined)
                          , (Endpoint2 i, Anticlockwise)
                          , (Marker2After i, undefined)
                          ]
                        | i <- Permutation.domain perm
                        ]
                    )
                ]
                -- Left-over boundaries that won't have any arcs connected to
                -- them. We track these by adding some artificial, negatively
                -- labeled markers.
                ++ [ BoundaryCircle [(Marker1Before (-i), undefined)] | i <- [1..r surface - 2] ]
            Same ->
                [ BoundaryCircle
                      $  (concat
                             [ [ (Marker1Before i, undefined)
                               , (Endpoint1 i, perm `Permutation.labelAt` i)
                               , (Marker1After i, undefined)
                               ]
                             | i <- Permutation.domain perm
                             ]
                         )
                      ++ (concat
                             [ [ (Marker2Before i, undefined)
                               , (Endpoint2 i, Anticlockwise)
                               , (Marker2After i, undefined)
                               ]
                             | i <- Permutation.domain perm
                             ]
                         )
                ]
                -- Left-over boundaries that won't have any arcs connected to
                -- them. We track these by adding some artificial, negatively
                -- labeled markers.
                ++ [ BoundaryCircle [(Marker1Before (-i), undefined)] | i <- [1..r surface - 1] ]
        )
        [ (Endpoint1 i, Endpoint2 (Permutation.permute perm i))
        | i <- Permutation.domain perm
        ]

-- | Computes the 'BoundaryId' of the boundary containing a given basepoint.
basepointToBoundaryId :: OrientedSystem Basepoint -> Basepoint -> BoundaryId
basepointToBoundaryId sys basepoint =
    boundaryIds !! fromJust (findIndex (elem basepoint) boundaries)
  where
    boundaries        = map (\(BoundaryCircle ls) -> map fst ls) $ basepoints sys
    boundariesMarkers = [[i | Marker1Before i <- b] | b <- boundaries]
    boundaryIds       =
      [ fromJust $ elemIndex marker representativeMarkersSorted
      | let representativeMarkers       = map minimum boundariesMarkers
      , let representativeMarkersSorted = sort representativeMarkers
      , marker <- representativeMarkers
      ]
-- | Finds the smallest @i@ such that the given boundary contains the basepoint
--   @Marker1Before i@.
boundaryIdToRepresentingMarker :: OrientedSystem Basepoint -> BoundaryId -> Int
boundaryIdToRepresentingMarker sys id = sort boundariesRepresentingMarker !! id
  where
    boundaries                   = map (\(BoundaryCircle ls) -> map fst ls) $ basepoints sys
    boundariesRepresentingMarker = [minimum [i | Marker1Before i <- b] | b <- boundaries]

-- | Decrements integer if it is above a threshold.
--
-- This is useful for getting rid of "holes" left by removing an integer:
--
-- >>> map (lower 4) ([1..3] ++ [5..10])
-- [1, 2, 3, 4, 5, 6, 7, 8, 9]
lower
    :: Int
    -- ^ Threshold \(n\)
    -> Int
    -- ^ Integer to be lowered \(m\) (throws exception if \(n = m\))
    -> Int
    -- ^ \(m - 1\) if \(n < m\) and \(m\) otherwise
lower n m | n < m     = m - 1
          | n > m     = m
          | otherwise = error "encountered removed level"

-- | The homomorphism \(H_1(\Gamma(S - a)) \to H_1(\Gamma(S))\)
addBridge
    :: Surface
    -- ^ Surface \(S - a\)
    -> Surface
    -- ^ Surface \(S\)
    -> BoundaryId
    -- ^ 'BoundaryId' in \(S\) of \(b_0\) of \(a\)
    -> BoundaryId
    -- ^ 'BoundaryId' in \(S\) of \(b_1\) of \(a\)
    -> BoundaryId
    -- ^ 'BoundaryId' in \(S - a\) of side that is clockwisely before the
    --   \(b_0\) end of \(a\)
    -> BoundaryId
    -- ^ 'BoundaryId' in \(S - a\) of side that is clockwisely after the
    --   \(b_0\) end of \(a\)
    -> (BoundaryId -> BoundaryId)
    -- ^ Map transforming 'BoundaryId's that are not on the newly created
    --   boundaries (that is, those 'BoundaryId's given as the 5th and 6th
    --   arguments respectively) from \(S - a\) to \(S\)
    -> Bool
    -- ^ Is the bridge twisted? That is, will curves with orientable complement
    --   in the subsurface have nonorientable complement in the supersurface?
    -> Homomorphism MCGElement MCGElement
    -- ^ Homomorphism \(H_1(\Gamma(S - a)) \to H_1(\Gamma(S))\)
addBridge sub super b0BoundarySuper b1BoundarySuper arcLeftBoundarySub arcRightBoundarySub otherBoundaryIdMap twist =
    mkHomomorphism (mcgAbelianization sub) (mcgAbelianization super) $
        computeH1ΓElement super .
        -- TODO: In the output, we are using generators that are not there.
        --       Instead, we should have functions that create the
        --       corresponding elements, a la 'boundaryTwist'.
        case (sub, super) of
            -- A non-twisted bridge between a boundary component to itself.
            (S g r, S g' r') | g == g' && r + 1 == r'
                               && b0BoundarySuper /= b1BoundarySuper
                               && arcLeftBoundarySub == arcRightBoundarySub ->
                \case
                    TwoSidedNonseparatingTwistOrientableComplement ->
                        1 ⋅ TwoSidedNonseparatingTwistOrientableComplement
                    BoundaryTwist b | b == arcLeftBoundarySub ->
                        boundaryTwist (S g' r') [b0BoundarySuper, b1BoundarySuper]
                    BoundaryTwist b -> boundaryTwist (S g' r') [otherBoundaryIdMap b]
                    DoubleBoundaryTwist b1 b2 ->
                        boundaryTwist (S g' r') $ concat
                            [ if b == arcLeftBoundarySub then
                                  [b0BoundarySuper, b1BoundarySuper]
                              else
                                  [otherBoundaryIdMap b]
                            | b <- [b1, b2]
                            ]
            -- A non-twisted bridge from a boundary component to itself.
            (N g r, N g' r') | g == g' && r + 1 == r'
                               && b0BoundarySuper /= b1BoundarySuper
                               && arcLeftBoundarySub == arcRightBoundarySub ->
                \case
                    TwoSidedNonseparatingTwistNonorientableComplement ->
                        1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
                    CrosscapTransposition ->
                        1 ⋅ CrosscapTransposition
                    TwoSidedNonseparatingTwistOrientableComplement ->
                        -- Since we do not change genus,
                        -- 'TwoSidedNonseparatingTwistOrientableComplement' is still a
                        -- generator.
                        1 ⋅ TwoSidedNonseparatingTwistOrientableComplement
                    BoundaryTwist b | b == arcLeftBoundarySub ->
                        boundaryTwist (N g' r') [b0BoundarySuper, b1BoundarySuper]
                    BoundaryTwist b -> boundaryTwist (N g' r') [otherBoundaryIdMap b]
                    DoubleBoundaryTwist b1 b2 ->
                        boundaryTwist (N g' r') $ concat
                            [ if b == arcLeftBoundarySub then
                                  [b0BoundarySuper, b1BoundarySuper]
                              else
                                  [otherBoundaryIdMap b]
                            | b <- [b1, b2]
                            ]
            -- A twisted bridge from a boundary component to itself.
            (N g r, N g' r') | g + 1 == g' && r == r'
                               && b0BoundarySuper == b1BoundarySuper
                               && arcLeftBoundarySub == arcRightBoundarySub ->
                \case
                    TwoSidedNonseparatingTwistNonorientableComplement ->
                        1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
                    CrosscapTransposition ->
                        1 ⋅ CrosscapTransposition
                    TwoSidedNonseparatingTwistOrientableComplement ->
                        1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
                    BoundaryTwist b | b == arcLeftBoundarySub ->
                        boundaryTwistPlus (N g' r') (N 1 1) [b0BoundarySuper]
                    BoundaryTwist b -> boundaryTwist (N g' r') [otherBoundaryIdMap b]
                    DoubleBoundaryTwist b1 b2
                        |  (b1 == arcLeftBoundarySub)
                        || (b2 == arcLeftBoundarySub) ->
                        boundaryTwistPlus (N g' r') (N 1 1) [b0BoundarySuper]
                    DoubleBoundaryTwist b1 b2 ->
                        boundaryTwist (N g' r') [otherBoundaryIdMap b1, otherBoundaryIdMap b2]
            -- A (non-twisted) bridge from a boundary component to another.
            -- Note: If one twists the bridge, the supersurface would be
            -- nonorientable (but have the same Euler characteristic and number
            -- of components). We do not handle this case because of the
            -- complement restrictions made in 'OrientedSystem'.
            (S g r, S g' r') | g + 1 == g' && r - 1 == r'
                               && b0BoundarySuper == b1BoundarySuper
                               && arcLeftBoundarySub /= arcRightBoundarySub ->
                \case
                    TwoSidedNonseparatingTwistOrientableComplement ->
                        1 ⋅ TwoSidedNonseparatingTwistOrientableComplement
                    BoundaryTwist b | b ∈ [arcLeftBoundarySub, arcRightBoundarySub] ->
                        1 ⋅ TwoSidedNonseparatingTwistOrientableComplement
                    BoundaryTwist b -> boundaryTwist (S g' r') [otherBoundaryIdMap b]
                    DoubleBoundaryTwist b1 b2
                        |  (arcLeftBoundarySub  ∈ [b1, b2])
                        && (arcRightBoundarySub ∈ [b1, b2]) ->
                        boundaryTwistPlus (S g' r') (S 1 1) [b0BoundarySuper]
                    DoubleBoundaryTwist b1 b2
                        |     (arcLeftBoundarySub  ∈ [b1, b2])
                        `xor` (arcRightBoundarySub ∈ [b1, b2]) ->
                        1 ⋅ TwoSidedNonseparatingTwistOrientableComplement
                    DoubleBoundaryTwist b1 b2 ->
                        boundaryTwist (S g' r') [otherBoundaryIdMap b1, otherBoundaryIdMap b2]
            -- A bridge from a boundary component to another.
            (N g r, N g' r') | g + 2 == g' && r - 1 == r'
                               && b0BoundarySuper == b1BoundarySuper
                               && arcLeftBoundarySub /= arcRightBoundarySub ->
                \case
                    TwoSidedNonseparatingTwistNonorientableComplement ->
                        1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
                    CrosscapTransposition ->
                        1 ⋅ CrosscapTransposition
                    TwoSidedNonseparatingTwistOrientableComplement ->
                        1 ⋅ if twist then TwoSidedNonseparatingTwistNonorientableComplement
                                     else TwoSidedNonseparatingTwistOrientableComplement
                    BoundaryTwist b | b ∈ [arcLeftBoundarySub, arcRightBoundarySub] ->
                        1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
                    BoundaryTwist b -> boundaryTwist (N g' r') [otherBoundaryIdMap b]
                    DoubleBoundaryTwist b1 b2
                        |  (arcLeftBoundarySub  ∈ [b1, b2])
                        && (arcRightBoundarySub ∈ [b1, b2]) ->
                        boundaryTwistPlus (N g' r') (S 1 1) [b0BoundarySuper]
                    DoubleBoundaryTwist b1 b2
                        |     (arcLeftBoundarySub  ∈ [b1, b2])
                        `xor` (arcRightBoundarySub ∈ [b1, b2]) ->
                        1 ⋅ TwoSidedNonseparatingTwistNonorientableComplement
                    DoubleBoundaryTwist b1 b2 ->
                        boundaryTwist (N g' r') [otherBoundaryIdMap b1, otherBoundaryIdMap b2]

data E_p1 cpx = E_p1 cpx

instance IndexedSemiSimplicialAbelianGroup (E_p1 NonseparatingArcs)
                                           (ModMCG NonseparatingArcs)
                                           (LabeledPermutation Orientation)
                                            MCGElement where

    indexing (E_p1 cpx) = cpx // MCG

    -- Not implemented for nonorientable surfaces
    overIndex (E_p1 (NonseparatingArcs config (S g r))) perm =
        mcgAbelianization (surface (fromJust (cutSystem sys)))
        where sys = twoBasepointsMarked (S g r) config perm

    faceHomomorphism i (E_p1 (NonseparatingArcs config (S g r))) perm =
        addBridge sub super b0BoundaryUncut b1BoundaryUncut arcLeftBoundaryCut arcRightBoundaryCut bridge false
      where
        sysFrom = twoBasepointsMarked (S g r) config perm
        sysTo   = twoBasepointsMarked (S g r) config (Permutation.face i perm)
        cut   = fromJust $ cutSystem sysFrom
        uncut = fromJust $ cutSystem sysTo
        sub = surface cut
        super = surface uncut
        b0BoundaryUncut = basepointToBoundaryId uncut (Marker1Before i)
        b1BoundaryUncut = basepointToBoundaryId uncut (Marker2Before (Permutation.permute perm i))
        arcLeftBoundaryCut  = basepointToBoundaryId cut (Marker1Before i)
        arcRightBoundaryCut = basepointToBoundaryId cut (Marker1After i)
        bridge boundaryIdCut = -- Picks out boundary intersecting after gluing. WARNING: Beware of ambiguity.
            basepointToBoundaryId
                uncut
                (Marker1Before (
                    lower i (boundaryIdToRepresentingMarker cut boundaryIdCut)
                ))

instance IndexedSemiSimplicialAbelianGroup (E_p1 NonseparatingNonorientableComplementArcs)
                                           (ModMCG NonseparatingNonorientableComplementArcs)
                                           (LabeledPermutation Orientation)
                                            MCGElement where

    indexing (E_p1 cpx) = cpx // MCG

    overIndex (E_p1 (NonseparatingNonorientableComplementArcs config (N g r))) perm =
        mcgAbelianization (surface (fromJust (cutSystem sys)))
        where sys = twoBasepointsMarked (N g r) config perm

    faceHomomorphism i (E_p1 (NonseparatingNonorientableComplementArcs config s)) perm =
        addBridge sub super b0BoundaryUncut b1BoundaryUncut arcLeftBoundaryCut arcRightBoundaryCut bridge
      where
        sysFrom = twoBasepointsMarked s config perm
        sysTo   = twoBasepointsMarked s config (Permutation.face i perm)
        cut   = fromJust $ cutSystem sysFrom
        uncut = fromJust $ cutSystem sysTo
        sub = surface cut
        super = surface uncut
        b0BoundaryUncut = basepointToBoundaryId uncut (Marker1Before i)
        b1BoundaryUncut = basepointToBoundaryId uncut (Marker2Before (Permutation.permute perm i))
        arcLeftBoundaryCut  = basepointToBoundaryId cut (Marker1Before i)
        arcRightBoundaryCut = basepointToBoundaryId cut (Marker1After i)
        bridge boundaryIdCut = -- Picks out boundary intersecting after gluing. WARNING: Beware of ambiguity.
            basepointToBoundaryId
                uncut
                (Marker1Before (
                    lower i (boundaryIdToRepresentingMarker cut boundaryIdCut)
                ))
