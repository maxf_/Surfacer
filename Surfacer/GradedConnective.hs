-- | \(\mathbb N_0\)-graded homotopy types.
module Surfacer.GradedConnective
    ( -- * Definitions.
      GradedConnective
      -- * Manipulating graded homotopy types.
    , (⊗)
    , Smash
    , (^^^)
    , Shifted
    , RegradedEven(RegradedEven)
      -- * Useful graded homotopy types.
    , OrientableMCGRelativeHomology(OrientableMCGRelativeHomology)
    )
where

import           Surfacer.Complex               ( Connectedness(Infinite, Only)
                                                )
import           Data.List

-- | Extracts the minimum of each diagonal.
minDiagonals :: (Num a, Ord a) => [[a]] -> [a]
minDiagonals [row] = row
minDiagonals (row1 : rest) =
    head row1 : zipWith min (tail row1) (minDiagonals rest)

-- | An \(\mathbb N_0\)-graded object with a connectivity estimate.
class GradedConnective a where
    -- | Connectivity estimates for each degree starting at degree \(0\).
    connGraded :: a -> [Connectedness]

-- | The smash product of two graded objects.
data Smash a b = Smash a b deriving Eq
-- | The smash product of two graded objects.
(⊗) = Smash
instance (Show a, Show b) => Show (Smash a b) where
    show (Smash a b) = show a ++ " \\otimes " ++ show b
instance (GradedConnective a, GradedConnective b) => GradedConnective (Smash a b) where
    connGraded (Smash a b) = minDiagonals
        -- The grid of smash products of @x@, @y@.
        [ [ max (x + y + Only 1) (Only (-2)) | y <- connGraded b ]
        | x <- connGraded a
        ]

-- | An \(\mathbb N_0\)-graded object with a connectivity estimate shifted up by
--   a given amount.
data Shifted a = Shifted Int a
-- | Shifts the connectivity estimate up by a given amount in each degree.
(^^^) :: GradedConnective a => a -> Int -> Shifted a
c ^^^ n = Shifted n c
instance Show a => Show (Shifted a) where
    show (Shifted n c) = "S^{0, " ++ show n ++ "} \\otimes " ++ show c
instance GradedConnective a => GradedConnective (Shifted a) where
    connGraded (Shifted n c) = map (+ Only n) (connGraded c)

-- | An \(\mathbb N_0\)-graded object which has been regraded to concentrate in
--   the even degrees and be contractible in the odd degrees.
newtype RegradedEven a = RegradedEven a
instance Show a => Show (RegradedEven a) where
    show (RegradedEven c) = "(" ++ show c ++ ")_{*/2}"
instance GradedConnective a => GradedConnective (RegradedEven a) where
    connGraded (RegradedEven c) = intersperse Infinite (connGraded c)

-- | The \(E_2\)-algebra of mapping class groups of orientable surfaces, graded
--   by genus, mod the generator \(\sigma \in H_{1, 0}(R)\).
--
-- The connectedness measures homological stability for \(\Gamma_{g, 1}\).
-- To be precise, the homology in degree \(g\) is
-- \(H_*(\Gamma_{g, 1}, \Gamma_{g - 1, 1})\).
--
-- This was considered by
-- <https://arxiv.org/abs/1805.07187 Galatius, Kupers, and Randal-Williams>
data OrientableMCGRelativeHomology = OrientableMCGRelativeHomology
instance Show OrientableMCGRelativeHomology where
    show _ = "\\overline R_{\\Gamma_{g, 1}} / \\sigma"
instance GradedConnective OrientableMCGRelativeHomology where
    -- GKRW, Theorem B(i)
    connGraded _ = [ Only ((2 * g - 1) `div` 3) | g <- [0 ..] ]
