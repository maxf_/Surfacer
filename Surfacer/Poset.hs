{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TemplateHaskell #-}

-- | Posets and numerical implementation of the nerve theorem.
module Surfacer.Poset
    ( -- * Posets
      Poset
    , (<<)
    , elements
    , Surfacer.Poset.isEmpty
    , Simplices(Simplices)
    , Simplex(Simplex)
    -- * Nerve theorem
    , NerveTheorem
    , NerveTheoremBox(NerveTheoremBox)
    , parameterizingPoset
    , subposetPoss
    , parametersContainingPoss
    , BoolExplained(True', FalseExplained)
    , nConnectedExplained
    , toBool
    )
where

import           Surfacer.Complex
import           Data.Function.Memoize

-- | A poset.
--
-- This encodes the input for the nerve theorem.
class Poset poset elem | poset -> elem where
    -- | The classes of elements.
    --
    -- The elements of the poset need to be classified into certain finite
    -- number of classes, which the operator '(<<)' factors through. This is
    -- possible for many posets appearing in nature, for example classiying
    -- elements by their orbits under a \(G\)-action.
    --
    -- The list should be empty if and only if the represented poset is empty.
    elements :: poset -> [elem]
    -- | Is the poset empty?
    --
    -- This is equivalent to @null . elements@.
    isEmpty :: poset -> Bool
    isEmpty = null . elements
    -- | The subposet below an element.
    (<<) :: poset
         -- ^ Poset \(\mathcal X\)
         -> elem
         -- ^ Element \(x \in \mathcal X\)
         -> ConnectiveBox
         -- ^ Subposet \(\mathcal X_{<x}\)

-- | The poset of simplices.
newtype Simplices cpx = Simplices cpx
-- | A \(p\)-simplex.
newtype Simplex = Simplex Int
instance Show Simplex where
    show (Simplex p) = show p ++ "-simplex"
instance Show cpx => Show (Simplices cpx) where
    show (Simplices cpx) = "\\mathrm{simp}(" ++ show cpx ++ ")"
-- | The poset of simplices is as connected as the underlying complex.
instance Connective cpx => Connective (Simplices cpx) where
    conn (Simplices c) = conn c
instance Complex cpx => Poset (Simplices cpx) Simplex where
    -- | Elements are simplices and they are classified by their dimension.
    elements (Simplices c) = [ Simplex p | p <- [0 .. dim c] ]
    -- | The poset below a \(p\)-simplex is \(\del \Delta^p\).
    _ << Simplex p = ConnectiveBox $ Simplices (Sphere (p - 1))

-- | A boolean together with an explanation in case of 'False'.
data BoolExplained = True'
                   -- ^ True.
                   | FalseExplained [Char]
                   -- ^ False with explanation.
instance Show BoolExplained where
    show True'              = "True"
    show (FalseExplained e) = "False with explanation: " ++ e
-- | Version of '(&&)' for 'BoolExplained'.
--
-- If one of the 'BoolExplained' are 'FalseExplained', it will return a
-- 'FalseExplained' with just the first explanation.
(&) :: BoolExplained -> BoolExplained -> BoolExplained
FalseExplained e & _     = FalseExplained e
True' & FalseExplained e = FalseExplained e
True' & True'            = True'

-- | Tests a boolean and attaches explanation if it is 'False'.
test :: Bool -> [Char] -> BoolExplained
test True  _ = True'
test False s = FalseExplained s
-- | Forgets the explanation.
toBool :: BoolExplained -> Bool
toBool True'              = True
toBool (FalseExplained _) = False

-- | A poset endowed with instructions to apply the nerve theorem.
--
-- Our preferred reference for the nerve theorem is
-- <https://arxiv.org/abs/1805.07187 Galatius--Kupers--Randal-Williams>,
-- Corollary 4.2. We use the same notation as they do, which we assume the
-- reader is familiar with.
class (Poset poset elem, Poset parameterizing parameter)
  => NerveTheorem poset elem parameterizing parameter
    | poset -> parameterizing where
    -- | The parameterizing poset \(\mathcal A\).
    parameterizingPoset :: poset -> parameterizing
    -- | The family of subposets \(F\).
    subposetPoss
        :: poset
        -- ^ Poset \(\mathcal X\)
        -> parameter
        -- ^ Parameter \(a \in \mathcal A\)
        -> [ConnectiveBox]
        -- ^ Possible values of \(F(a)\) (\(F(a)\) might not be uniquely
        --   determined by the class of \(a\))
    -- | The subposets \(\mathcal A_x\).
    parametersContainingPoss
        :: poset
        -- ^ Poset \(\mathcal X\)
        -> elem
        -- ^ Element \(x \in \mathcal X\)
        -> [ConnectiveBox]
        -- ^ Possible values of \(\mathcal A_x\) (\(\mathcal A_x\) might not be
        --   uniquely determined by the class of \(a\))

    -- | Is the poset \(n\)-connected according to the nerve theorem.
    --
    -- Assuming that the functions of 'NerveTheorem' are implemented correctly,
    -- we have that if this returns 'True', it is provably \(n\)-connected.
    nConnected :: (Connective parameterizing) => poset -> Int -> Bool
    -- \((-2)\)-connectedness is vacuous.
    nConnected _ (-2) = True
    -- \((-1)\)-connectedness is equivalent to nonemptiness.
    nConnected p (-1) = not (Surfacer.Poset.isEmpty p)
    nConnected p n =
        -- Tests hypothesis (a).
        Only n <= conn (parameterizingPoset p)
        && and
               -- Tests hypothesis (b).
               [ Only n - taa <= conn fa
               | a  <- elements (parameterizingPoset p)
               , let aa  = parameterizingPoset p << a
               , let taa = conn aa + Only 2
               , fa <- subposetPoss p a
               ]
        && and
               -- Tests hypothesis (c).
               [ Only n - txx - Only 1 <= conn ax
               | x  <- elements p
               , let xx  = p << x
               , let txx = conn xx + 2
               , ax <- parametersContainingPoss p x
               ]

    -- | Version of 'nConnected' where the result is augmented in the case of
    --   failure by an explanation of which inequality failed.
    --
    -- We found this to be extremely helpful for constructing nerve theorem
    -- arguments, as the tedious matter of invalidating inequalities is
    -- deferred to a computer.
    nConnectedExplained
        :: (Show elem, Show poset, Show parameter, Connective parameterizing)
        => poset
        -> Int
        -> BoolExplained
    -- This is implemented as 'nConnected'.
    nConnectedExplained _ (-2) = True'
    nConnectedExplained p (-1) = test (not (Surfacer.Poset.isEmpty p)) "poset is empty"
    nConnectedExplained p n =
        -- Tests hypothesis (a).
        test
            (Only n <= conn (parameterizingPoset p))
            (  "(a) failed for showing that "
            ++ show p
            ++ " is "
            ++ show n
            ++ "-connected"
            )
        & foldl (&)
                True'
                -- Tests hypothesis (b).
                [ test
                      (Only n - taa <= conn fa)
                      (  "(b) failed for showing that "
                      ++ show p
                      ++ " is "
                      ++ show n
                      ++ "-connected when a="
                      ++ show a
                      ++ ", F(a)="
                      ++ show fa
                      ++ " ("
                      ++ show (conn fa)
                      ++ "-connected), \\mathcal A_{<a}="
                      ++ show aa
                      ++ " ("
                      ++ show (conn aa)
                      ++ "-connected)"
                      )
                | a  <- elements (parameterizingPoset p)
                , let aa  = parameterizingPoset p << a
                , let taa = conn aa + Only 2
                , fa <- subposetPoss p a
                ]
        & foldl (&)
                True'
                -- Tests hypothesis (c).
                [ test
                      (Only n - txx - Only 1 <= conn ax)
                      (  "(c) failed for showing that "
                      ++ show p
                      ++ " is "
                      ++ show n
                      ++ "-connected when x="
                      ++ show x
                      ++ ", \\mathcal A_x="
                      ++ show ax
                      ++ " ("
                      ++ show (conn ax)
                      ++ "-connected), \\mathcal X_{<x}="
                      ++ show xx
                      ++ " ("
                      ++ show (conn xx)
                      ++ "-connected)"
                      )
                | x  <- elements p
                , let xx  = p << x
                , let txx = conn xx + 2
                , ax <- parametersContainingPoss p x
                ]

-- | A container providing 'Connective' for types implementing 'NerveTheorem'.
newtype NerveTheoremBox poset elem parameterizing parameter = NerveTheoremBox poset
deriveMemoizableParams ''NerveTheoremBox [1]
instance Show poset => Show (NerveTheoremBox poset elem parameterizing parameter) where
    show (NerveTheoremBox poset) = show poset
instance Complex poset => Complex (NerveTheoremBox poset elem parameterizing parameter) where
    dim (NerveTheoremBox p) = dim p
instance (Connective parameterizing, NerveTheorem poset elem parameterizing parameter, Memoizable poset)
  => Connective (NerveTheoremBox poset elem parameterizing parameter) where
    -- | To improve performance, we memoize the result.
    --
    -- The effects of this are quite severe, as we rely on recursion.
    conn = memoize
        -- Repeatedly test the nerve theorem argument until we reach a
        -- connectedness where it fails.
        (\(NerveTheoremBox p) ->
            let (conns, _) = span (nConnected p) [-2 ..] in Only $ last conns
        )
