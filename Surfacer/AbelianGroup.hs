{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TupleSections #-}

module Surfacer.AbelianGroup
    ( -- * Abelian groups
      Cyclic(Z, ℤ)
    , invariantFactor
    , AbGrp
    , trivial
    , (⇜)
    , (⊕)
    , summands
    , generators
    , mapLabels
      -- * Elements
    , LinearCombination(LinearCombination)
    , terms
    , zero
    , (⋅)
    , scale
    , minus
    , simplify
    , project
    , zeroIn
    , equalIn
    , orderIn
      -- * Homomorphisms
    , Homomorphism
    , mkHomomorphism
    , source
    , target
    , (∘)
    , scaleHomomorphism
      -- * Tensoring and linear algebra
    , Field(ℚ, 𝔽)
    , reduce
    , matrix
      -- * Simplicial abelian groups
    , IndexedSemiSimplicialAbelianGroup(..)
      -- * SAGE and LaTeX integration
    , latexNumber
    , ToSage
    )
where

import Prelude hiding ((++))
import Data.List hiding ((++))
import Data.Maybe
import Control.Monad
import Control.Applicative
import Control.Arrow
import Test.QuickCheck hiding (scale)
import Surfacer.Orbits hiding (differential)
import Data.Functor.Classes

-- | Converts a number to a LaTeX expression, wrapping it in curly brackets if it exceeds one digit.
latexNumber :: Int -> [Char]
latexNumber n
    | n >= 0 && n <= 9
    = show n
    | otherwise
    = "{" ++ show n ++ "}"

-- | An object which can be represented in SAGE.
class ToSage a where
    -- | Converts a value to a Python expression that can be run with SAGE.
    sage :: a -> [Char]

-- | A cyclic group (up to isomorphism).
data Cyclic = Z' Int
            -- ^ The cyclic group of finite order \(p\)
            | ℤ deriving Eq
            -- ^ The cyclic group of infinite order.
pattern Z :: Int -> Cyclic
pattern Z n <- Z' n where
    Z n | n < 0     = error "invalid group order"
        | otherwise = Z' n

-- | Extracts the \(n\) such that the group takes the form \(\Z/n\Z\).
invariantFactor :: Cyclic -> Int
invariantFactor (Z n) = n
invariantFactor ℤ     = 0

instance Show Cyclic where
    show ℤ     = "\\Z"
    show (Z 0) = "0"
    show (Z n) = "Z_" ++ latexNumber n

-- | A finitely generated abelian group (up to isomorphism) with labeled cyclic summands.
newtype AbGrp label = AbGrp [(Cyclic, label)]
    deriving Eq

-- | The trivial group.
trivial :: AbGrp label
trivial = AbGrp []
-- | Attaches a label to a cyclic group.
(⇜) :: Cyclic -> label -> AbGrp label
z ⇜ g = AbGrp [(z, g)]
infixl 9 ⇜
-- | Takes the direct sum of two abelian groups.
(⊕) :: Eq label => AbGrp label -> AbGrp label -> AbGrp label
infixl 6 ⊕
AbGrp a ⊕ AbGrp b
    | not $ null $ map snd a `intersect` map snd b
    = error "cannot use same label for more than one cylic summand of abelian group"
    | otherwise
    = AbGrp (a ++ b)
-- | Lists the labels of the cyclic summands.
generators :: AbGrp label -> [label]
generators (AbGrp ls) = map snd ls
-- | Lists the summands and their labels.
summands :: AbGrp label -> [(Cyclic, label)]
summands (AbGrp xs) = xs
-- | Finds the invariant factor (see 'invariantFactor') of a cyclic summand with a given label.
invariantFactorOf :: Eq label => AbGrp label -> label -> Int
invariantFactorOf (AbGrp ls) g = invariantFactor $ fst $ fromJust $ find ((== g) . snd) ls
-- | Map labels along an injective lambda.
mapLabels :: Eq label2 => (label1 -> label2) -> AbGrp label1 -> AbGrp label2
mapLabels f (AbGrp ls) = mconcat [z ⇜ f g | (z, g) <- ls]

instance Eq label => Semigroup (AbGrp label) where
    (<>) = (⊕)
instance Eq label => Monoid (AbGrp label) where
    mempty = trivial

instance Show label => Show (AbGrp label) where
    show (AbGrp [(z, g)]) = show z ++ " " ++ show g
    show (AbGrp (x:xs))   = show (AbGrp [x]) ++ " \\oplus " ++ show (AbGrp xs)
instance ToSage (AbGrp label) where
    sage (AbGrp ls) =
        free
     ++ " / (" ++ free ++ ").span(["
     ++ intercalate
            ", "
            [ show (invariantFactor z)
              ++ " * (" ++ free ++ ")." ++ show n
            | (n, (z, _)) <- zip [0..] ls]
     ++ "])"
     where free = "ZZ^" ++ show (length ls)

-- TODO: This should include the group so that you can determine equality
-- | A linear combination with integer coefficients.
--
-- A linear combination is represented as a list of pairs of integer
-- coefficients and labels. Thus, it is kept in an unsimplified form.
newtype LinearCombination label = LinearCombination [(Int, label)]
    deriving (Semigroup, Monoid, Functor)

-- | Extracts the terms of a linear combination, possibly with multiple terms
--   that have the same label.
terms :: LinearCombination label -> [(Int, label)]
terms (LinearCombination ls) = ls
-- | The trivial linear combination.
zero :: LinearCombination label
zero = LinearCombination []
-- | Creates a linear combination with just one term.
(⋅) :: Int -> label -> LinearCombination label
infix 9 ⋅
0 ⋅ _ = zero
n ⋅ g = LinearCombination [(n, g)]
-- | Multiply all coefficients by an integer.
scale :: Int -> LinearCombination label -> LinearCombination label
scale n (LinearCombination ls) = LinearCombination $ map (first (n *)) ls
-- | Flip the signs of all coefficients.
minus :: LinearCombination label -> LinearCombination label
minus = scale (-1)
-- | Simplifies a linear combination by adding up coefficients associated to
--   equal labels and sorts the terms by labels.
simplify :: Ord label => LinearCombination label -> LinearCombination label
simplify (LinearCombination ls) = LinearCombination $ simplify' $ sortOn snd ls
  where
    simplify' []                                = []
    simplify' ((0, g) : xs)                     = simplify' xs
    simplify' ((n, g) : (m, g') : xs) | g == g' = simplify' ((n + m, g) : xs)
    simplify' ((n, g) : xs)                     = (n, g) : simplify' xs
-- | Extracts the coefficients to a given label.
project :: Eq label => label -> LinearCombination label -> Int
project g (LinearCombination ls) = sum [n | (n, g') <- ls, g' == g]
-- | Is this linear combination trivial in this abelian group?
zeroIn :: Ord label => LinearCombination label -> AbGrp label -> Bool
x `zeroIn` a = zero' $ terms $ simplify x
  where
    zero' [] = True
    zero' ((n, x) : xs)
        | a `invariantFactorOf` x == 0 = n                                 == 0 && zero' xs
        | otherwise                    = n `mod` (a `invariantFactorOf` x) == 0 && zero' xs
-- | Are these two linear combinations equal in this abelian group?
equalIn :: Ord label => AbGrp label -> LinearCombination label -> LinearCombination label -> Bool
equalIn a x y = x ++ minus y `zeroIn` a
-- | Computes the order of an element in an abelian group.
orderIn :: Ord label => LinearCombination label -> AbGrp label -> Int
x `orderIn` a =
    order (terms $ simplify x)
  where
    order []          = 1
    order [(0, _)]    = 0
    order ((n, g):xs) = lcm ((a `invariantFactorOf` g) `div` gcd n (a `invariantFactorOf` g))
                            (order xs)

instance (Show label, Ord label) => Show (LinearCombination label) where
    show = show' . terms . simplify
      where
        show' [(1, g)] = show g
        show' [(n, g)] = show n ++ " " ++ show g
        show' ((n, g) : (m, g') : xs)
            | m > 0 = show' [(n, g)] ++ " + " ++ show' (( m, g') : xs)
            | m < 0 = show' [(n, g)] ++ " - " ++ show' ((-m, g') : xs)

instance Applicative LinearCombination where
    pure = (1 ⋅)
    (<*>) = ap
instance Monad LinearCombination where
    LinearCombination ls >>= f = mconcat $ map (\(n, g) -> scale n (f g)) ls

(++) :: Monoid m => m -> m -> m
(++) = mappend

-- | A homomorphism between two abelian groups.
data Homomorphism label1 label2 = Homomorphism (AbGrp label1)
                                               (AbGrp label2)
                                               (label1 -> LinearCombination label2)

-- | Constructs a homomorphism.
--
-- We use this to escape pattern matching which could expose the lambda
-- @label1 -> LinearCombination label2@ thus encoding more information than
-- necessary for the mathematical notion of a homomorphism.
--
-- This throws an exception when one tries to use it to create a homomorphism
-- which does not respect orders of elements.
mkHomomorphism
    :: Ord label2
    => AbGrp label1
    -- ^ Source abelian group
    -> AbGrp label2
    -- ^ Target abelian group
    -> (label1 -> LinearCombination label2)
    -- ^ Lambda computing what a given generator is mapped to
    -> Homomorphism label1 label2
    -- ^ Resulting homomorphism
mkHomomorphism source target f
    | not $ all
        (\(factor, label) -> (invariantFactor factor) `mod` ((f label) `orderIn` target) == 0)
        (summands source)
    = error "non-homomorphic mapping (fails to respect element orders)"
    | otherwise
    = Homomorphism source target f
-- | The source of a homomorphism.
source :: Homomorphism label1 label2 -> AbGrp label1
source (Homomorphism s _ _) = s
-- | The target of a homomorphism.
target :: Homomorphism label1 label2 -> AbGrp label2
target (Homomorphism _ t _) = t
-- | Evaluates the homomorphism on a generator.
eval :: Homomorphism label1 label2 -> label1 -> LinearCombination label2
eval (Homomorphism _ _ h) = h
-- | Composes two homomorphisms.
(∘) :: Eq label2
    => Homomorphism label2 label3
    -> Homomorphism label1 label2
    -> Homomorphism label1 label3
Homomorphism s1 t1 h1 ∘ Homomorphism s2 t2 h2
    | t2 /= s1
    = error "composing homomorphisms with incompatible function signatures"
    | otherwise
    = Homomorphism s2 t1 (h1 <=< h2)
-- | Multiplies a homomorphism by a factor.
scaleHomomorphism :: Int -> Homomorphism label1 label2 -> Homomorphism label1 label2
scaleHomomorphism n (Homomorphism s t h) = Homomorphism s t (scale n . h)

instance (Eq label1, Ord label2) => Eq (Homomorphism label1 label2) where
    h1 == h2
        | (source h1, target h1) /= (source h2, target h2)
        = error "comparing homomorphism with incompatible source/target"
        | otherwise
        = liftEq (equalIn (target h1))
                 (map (eval h1) (generators (source h1)))
                 (map (eval h2) (generators (source h1)))

-- | Creates SAGE representation of homomorphism.
sageHomomorphism
    :: Eq label2
    => [Char]
    -- ^ Expression for source
    -> [Char]
    -- ^ Expression for target
    -> Homomorphism label1 label2
    -- ^ Homomorphism
    -> [Char]
    -- ^ SAGE representation of homomorphism.
sageHomomorphism s t h =
        s ++ ".hom(["
        ++ intercalate
               " + "
               [ "gen.lift()[" ++ show i ++ "] * "
                  ++ t ++ "(vector(["
                  ++ intercalate
                         ", "
                         [ show $ project g' (eval h g)
                         | g' <- generators $ target h
                         ]
                  ++ "]))"
               | (i, g) <- zip [0..] $ generators (source h)
               ]
        ++ " for gen in " ++ s ++ ".gens()])"

instance Eq label2 => ToSage (Homomorphism label1 label2) where
    sage h =
        "s = " ++ sage (source h) ++ "\n"
     ++ "t = " ++ sage (target h) ++ "\n"
     ++ sageHomomorphism "s" "t" h

-- | Tensors abelian group with finite field of prime order.
reduce
    :: Ord label
    => Int
    -- ^ Prime number.
    -> AbGrp label
    -- ^ Abelian group.
    -> AbGrp label
    -- ^ Tensored abelian group.
reduce p a = mconcat
    [ Z p ⇜ g
    | g <- generators a
    , gcd (a `invariantFactorOf` g) p /= 1
    ]

-- | A field.
data Field = ℚ
           -- ^ Field of rational numbers.
           | 𝔽 Int
           -- ^ Finite field of prime order.
-- | Computes the matrix corresponding to a homomorphism tensored with a field.
matrix
    :: (Ord label1, Ord label2)
    => Field
    -- ^ Field to tensor with
    -> Homomorphism label1 label2
    -- ^ Homomorphism
    -> [[Int]]
    -- ^ Matrix where the outer list is parameterized over the generators of
    --   the source (excluding those that die from tensoring) and the inner
    --   list over the generators of the target (excluding those that die from
    --   tensoring)
matrix (𝔽 p) h =
    [ [ project g' (eval h g) `mod` p
      | g' <- generators $ reduce p $ target h
      ]
    | g <- generators $ reduce p $ source h
    ]
matrix ℚ h =
    [ [ project g' (eval h g)
      | g' <- generators $ target h
      , target h `invariantFactorOf` g' == 0
      ]
    | g <- generators $ source h
    , source h `invariantFactorOf` g == 0
    ]
-- | Checks the validity of the dimensions of a matrix and returns back the matrix.
check :: [[a]] -> [[a]]
check m
    | map length m /= replicate (length m) (length (head m))
    = error "invalid matrix"
    | otherwise
    = m
-- | The columns and rows of a matrix.
dimensions :: [[a]] -> (Int, Int)
dimensions m = (length (check m), length (head m))
-- | Multiplies a matrix by a scalar.
(|⋅|) :: Num a => a -> [[a]] -> [[a]]
λ |⋅| m = map (map (λ *)) (check m)
-- | Sums two matrices.
(|+|) :: Num a => [[a]] -> [[a]] -> [[a]]
m1 |+| m2
    | dimensions m1 /= dimensions m2
    = error "cannot sum matrices of different dimensions"
    | otherwise
    = zipWith (zipWith (+)) m1 m2
-- | Multiplies two matrices.
(|*|) :: Num a => [[a]] -> [[a]] -> [[a]]
m1 |*| m2
    | snd (dimensions m2) /= fst (dimensions m1)
    = error "invalid matrix multiplication; unmatching dimensions"
    | otherwise
    = [ [ sum $ zipWith (*) m1r m2c | m1r <- transpose m1 ] | m2c <- m2 ]
-- | Creates a zero matrix with a certain number of columns and rows.
zeroMatrix :: Int -> Int -> [[Int]]
zeroMatrix columns rows = [[0 | _ <- [1..rows]] | _ <- [1..columns]]
-- | Replaces every entry in a matrix by zeros.
zeroify :: [[Int]] -> [[Int]]
zeroify = map (map (const 0)) . check
-- | Flattens a matrix of matrices.
flatten :: [[[[a]]]] -> [[a]]
flatten m = mconcat
    [ foldl' (zipWith (++)) (replicate colWidth []) col
    | col <- check m
    , let colWidth = length (head col)
    ]

-- TODO: extend to i = 0

-- | A semi-simplicial abelian group consisting of direct sums indexed over a
--   semi-simplicial set.
class (SemiSimplicialSet indexing index, Ord label)
     => IndexedSemiSimplicialAbelianGroup svs indexing index label
      | svs -> indexing label where
    -- | The indexing semi-simplicial set.
    indexing :: svs -> indexing
    -- | The summand at a given index.
    overIndex :: svs -> index -> AbGrp label
    -- | The face homomorphism restricted to the summand at a given index.
    faceHomomorphism :: Int -> svs -> index -> Homomorphism label label

    -- | Computes the dimension for the summand over a given indexed reduced
    --   over a field.
    dimensionFor :: Field -> svs -> index -> Int
    dimensionFor (𝔽 p) s σ = length $ generators $ reduce p (overIndex s σ)
    dimensionFor  ℚ    s σ = length $ filter ((== ℤ) . fst) summands
        where AbGrp summands = overIndex s σ

    -- | Computes the abelian group at a given degree.
    simplicesAbelianGroup :: Int -> svs -> AbGrp (Int, label)
    simplicesAbelianGroup p s = mconcat [ mapLabels (i,) $ overIndex s σ
                                        | (i, σ) <- zip [0..] $ simplices p (indexing s)
                                        ]
    -- | Computes the differential for the Moore chain complex.
    differential :: Eq index => Int -> svs -> Homomorphism (Int, label) (Int, label)
    differential p s = mkHomomorphism source target $
        \(r, g) -> mconcat [ (d_iσ,) <$> scale ((-1)^i) (eval d_ih g)
                           | let σ = xp !! r
                           , i <- [0 .. simplexDim idx σ]
                           , let d_iσ = fromJust $ elemIndex (face i idx σ) xpMinus1
                           , let d_ih = faceHomomorphism i s σ
                           ]
      where
        idx = indexing s
        xpMinus1 = simplices (p - 1) (indexing s)
        xp       = simplices p (indexing s)
        source = simplicesAbelianGroup  p      s
        target = simplicesAbelianGroup (p - 1) s
    -- | Computes the matrix for the differential for the Moore chain complex
    --   with coefficients in a given field.
    differentialMatrix :: Eq index => Field -> Int -> svs -> [[Int]]
    differentialMatrix field p s = flatten (differentialUnflattened field p s)
      where
        idx = indexing s
        x0  = simplices 0 idx
        differentialUnflattened _ 0 s = map (const []) x0
        differentialUnflattened field p s =
            [ col column | (column, _) <- zip [0..] xp ]
          where
            idx        = indexing s
            xp         = simplices p       idx -- \(X_p\)
            xpMinus1   = simplices (p - 1) idx -- \(X_{p - 1}\)
            -- Calculates the column at @column@.
            col column =
                -- Compute the alternating sum of faces.
                let σ        = xp !! column
                    summands = [ [ if τ == d_iσ then
                                       ((-1) ^ i) |⋅| matrix field (faceHomomorphism i s σ)
                                   else
                                       zeroMatrix (dimensionFor field s σ)
                                                  (dimensionFor field s τ)
                                 | τ <- xpMinus1
                                 ]
                               | i <- [0 .. p]
                               , let d_iσ = face i idx σ
                               ]
                in  foldl' (zipWith (|+|)) (map zeroify (head summands)) summands
    -- | Prints SAGE code for computing the homology of the Moore chain complex.
    sageHomology :: Eq index => Int -> svs -> [Char]
    sageHomology p s =
           "a = " ++ sage (simplicesAbelianGroup (p + 1) s) ++ "\n"
        ++ "b = " ++ sage (simplicesAbelianGroup  p      s) ++ "\n"
        ++ "c = " ++ sage (simplicesAbelianGroup (p - 1) s) ++ "\n"
        ++ "phi1 = " ++ sageHomomorphism "a" "b" (differential (p + 1) s) ++ "\n"
        ++ "phi2 = " ++ sageHomomorphism "b" "c" (differential  p      s) ++ "\n"
        ++ "phi2.kernel() / phi1.image()"

    -- | Tests that the simplicial identities hold.
    prop_SimplicialIdentities
        :: Eq index
        => svs
        -> index
        -> Property
    prop_SimplicialIdentities s σ = p >= 2 && σ `elem` simplices p idx ==>
        do
            i <- chooseInt (0, p - 1)
            j <- chooseInt (i + 1, p)
            return $ printTestCase ("(i,j) = " ++ show (i, j))
                                   (faceHomomorphism i s (face j idx σ) ∘ faceHomomorphism j s σ
                                 == faceHomomorphism (j - 1) s (face i idx σ) ∘ faceHomomorphism i s σ)
      where
        idx = indexing s
        p = simplexDim idx σ
