{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}

-- | Surfaces and functions for manipulating them.
module Surfacer.Surface
    ( Surface(S, N)
    , isOrientable
    , isNonorientable
    , addBoundaries
    , g
    , r
    , chi
    , (⊕)
    , (#)
    , decompositions
    , orientableCover
    )
where

import           Data.Hashable
import           Data.Function.Memoize
import           Test.QuickCheck

-- | A diffeomorphism type of a connected surface, that is, a connected surface
--   up to diffeomorphism.
--
-- 'Surface' is the main primitive of the Surfacer library. It keeps the genus
-- and number of boundary components of a given surface, which by the
-- classification of surfaces determines and is determined by the
-- diffeomorphism type.
--
-- It is guaranteed, by runtime checks, that both the genus and number of
-- boundary components are nonnegative values, so that the values indeed
-- represent valid diffeomorphism types. In addition, @N 0 r@ is guaranteed to
-- not occur, so as to avoid duplicate representations for the surfaces
-- \(S_{0, r}\).
data Surface = S' Int Int
            -- ^ An orientable surface up to diffeomorphism, specified by
            --   @(genus, number of boundary components)@.
             | N' Int Int
            -- ^ A nonorientable surface up to diffeomorphism, specified by
            --   @(genus, number of boundary components)@.
    deriving (Ord, Eq)

deriveMemoizable ''Surface

-- | Produces a nonorientable or genus 0 surface.
--
-- If genus \(g\) is 0, it produces \(S_{0, r}\).
pattern N
    :: Int
    -- ^ Genus \(g \geq 0\) (throws exception if not satisfied)
    -> Int
    -- ^ Number of boundary components \(r \geq 0\) (throws exception if not
    --   satisfied)
    -> Surface
    -- ^ Surface \(N_{g, r}\)
pattern N g r <- N' g r where
    N 0 r = S 0 r
    N g r | g < 0     = error "negative g"
          | r < 0     = error "negative r"
          | otherwise = N' g r
-- | Produces an orientable surface.
pattern S
    :: Int
    -- ^ Genus \(g \geq 0\) (throws exception if not satisfied)
    -> Int
    -- ^ Number of boundary components \(r \geq 0\) (throws exception if not
    --   satisfied)
    -> Surface
    -- ^ Surface \(S_{g, r}\)
pattern S g r <- S' g r where
    S g r | g < 0     = error "negative g"
          | r < 0     = error "negative r"
          | otherwise = S' g r

-- | Is the surface orientable?
isOrientable :: Surface -> Bool
isOrientable (S _ _) = True
isOrientable (N _ _) = False
-- | Is the surface nonorientable?
--
-- This is equivalent to @not . isOrientable@.
isNonorientable :: Surface -> Bool
isNonorientable = not . isOrientable

instance Hashable Surface where
    hashWithSalt s (S g r) = hash (s, 0 :: Int, g, r)
    hashWithSalt s (N g r) = hash (s, 1 :: Int, g, r)
instance Show Surface where
    show (S g r) = "S_{" ++ show g ++ ", " ++ show r ++ "}"
    show (N g r) = "N_{" ++ show g ++ ", " ++ show r ++ "}"
instance Arbitrary Surface where
    arbitrary = do
        NonNegative g <- arbitrary
        NonNegative r <- arbitrary
        orientable <- arbitrary
        return $ if orientable then S g r else N g r
    -- To 'shrink' a surface, we just 'shrink' its genus and its boundary
    -- separately using the implementations in 'QuickCheck', and take the
    -- cartesian product of the values.
    shrink (S g r) =
        [ S g' r'
        | g' <- map getNonNegative $ shrink $ NonNegative g
        , r' <- map getNonNegative $ shrink $ NonNegative r
        ]
    shrink (N g r) =
        [ N g' r'
        | g' <- map getNonNegative $ shrink $ NonNegative g
        , r' <- map getNonNegative $ shrink $ NonNegative r
        ]

-- | The genus of a surface.
--
-- Beware! Genus has to different, not so compatible, meanings depending on
-- whether the surface is orientable or nonorientable. For \(S_{g, r}\) and
-- \(N_{g, r}\), this function produces \(g\).
g :: Surface -> Int
g (S g _) = g
g (N g _) = g

-- | The number of boundary components of a surface.
r :: Surface -> Int
r (S _ r) = r
r (N _ r) = r

-- | The Euler characteristic of a surface.
chi :: Surface -> Int
chi (S g r) = 2 - 2 * g - r
chi (N g r) = 2 -     g - r

-- | Juxtaposition of surfaces.
--
-- This operation juxtaposes two surfaces, by picking a distinguished boundary
-- component in each surface and gluing the two surfaces together along
-- embedded intervals in these boundary components.
--
-- If one of the surfaces are closed (that is, have no boundary components), it
-- throws an exception.
--
-- >>> (N 4 2) ⊕ (N 2 1)
-- N_{6, 2}
(⊕) :: Surface -> Surface -> Surface
(N g1 r1) ⊕ (N g2 r2)
    | r1 == 0 || r2 == 0 = error "juxtaposing without a boundary"
    | otherwise          = N (g1 + g2) (r1 + r2 - 1)
(N g1 r1) ⊕ (S g2 r2)
    | r1 == 0 || r2 == 0 = error "juxtaposing without a boundary"
    | otherwise          = N (g1 + 2 * g2) (r1 + r2 - 1)
(S g1 r1) ⊕ (N g2 r2)
    | r1 == 0 || r2 == 0 = error "juxtaposing without a boundary"
    | otherwise          = N (2 * g1 + g2) (r1 + r2 - 1)
(S g1 r1) ⊕ (S g2 r2)
    | r1 == 0 || r2 == 0 = error "juxtaposing without a boundary"
    | otherwise          = S (g1 + g2) (r1 + r2 - 1)

-- | Tests that the Euler characteristic is additive with respect to
--   juxtaposition.
_prop_EulerCharacteristicAdditive x y = r x >= 1 && r y >= 1 ==>
    chi (x ⊕ y) == chi x + chi y

-- | Connected sum of surfaces.
--
-- >>> (N 4 2) ⊕ (N 2 1)
-- N_{6, 3}
(#) :: Surface -> Surface -> Surface
(N g1 r1) # (N g2 r2) = N (g1 + g2) (r1 + r2)
(N g1 r1) # (S g2 r2) = N (g1 + 2 * g2) (r1 + r2)
(S g1 r1) # (N g2 r2) = N (2 * g1 + g2) (r1 + r2)
(S g1 r1) # (S g2 r2) = S (g1 + g2) (r1 + r2)

-- | Decompositions of a surface '(⊕)' under.
--
-- This produces the list of all pairs of surfaces that juxtaposes to a given
-- surface. Such pairs may include the identity \(S_{0, 1}\).
decompositions
    :: Surface
    -- ^ Surface \(S\) (throws exception if it has no boundary components)
    -> [(Surface, Surface)]
    -- ^ List of decompositions (no duplicates)
decompositions (S _ r) | r < 1 =
    error "we need a distinguished boundary component but r < 1"
decompositions (S g r) =
    [ (S g0 r0, S g1 r1)
    | g0 <- [0 .. g]
    , let g1 = g - g0
    , r0 <- [1 .. r]
    , let r1 = r - r0 + 1
    ]
decompositions (N g r)
    | r < 1
    = error "we need a distinguished boundary component but r < 1"
    | otherwise
    =
    [ (N g0 r0, N g1 r1)
    | g0 <- [0 .. g]
    , let g1 = g - g0
    , r0 <- [1 .. r]
    , let r1 = r - r0 + 1
    ]
 ++ [ (S g0 r0, N g1 r1)
    | g0 <- [0 .. (g - 1) `div` 2]
    , let g1 = g - 2 * g0
    , r0 <- [1 .. r]
    , let r1 = r - r0 + 1
    ]
 ++ [ (N g0 r0, S g1 r1)
    | g1 <- [0 .. (g - 1) `div` 2]
    , let g0 = g - 2 * g1
    , r1 <- [1 .. r]
    , let r0 = r - r1 + 1
    ]

-- | Tests that juxtaposition undoes decomposition.
_prop_JuxtapositionDecomposition surface =
    r surface >= 1 ==>
        all ((== surface) . uncurry (⊕)) (decompositions surface)

-- | Adds boundary components to a surface.
addBoundaries
    :: Int
    -- ^ Change in boundary components \(n\) (may be negative)
    -> Surface
    -- ^ \(S_{g, r}\) (respectively \(N_{g, r}\))
    -> Surface
    -- ^ \(S_{g, r + n}\) (respectively \(N_{g, r + n}\))
addBoundaries n (S g r) = S g (r + n)
addBoundaries n (N g r) = N g (r + n)

-- | The orientable double cover a surface.
--
-- If the surface is already orientable, nothing is changed.
orientableCover
    :: Surface
    -- ^ Surface \(S\)
    -> Surface
    -- ^ Surface \(S\) or the orientable cover \(\tilde S\) of \(S\)
orientableCover surface@(S _ _) = surface
orientableCover (N g r) = S (g - 1) (2 * r)

-- | Tests that taking the double cover of a nonorientable surface doubles the
--   Euler characteristic.
_prop_EulerCharacteristicCover surface = isNonorientable surface ==>
    chi (orientableCover surface) == 2 * chi surface
