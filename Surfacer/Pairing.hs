{-# LANGUAGE PatternSynonyms #-}

-- | Pairings.
module Surfacer.Pairing
    ( Pairing(Pairing)
    , members
    , numPairs
    , allPairings
    , face
    , friend
    )
where

import           Data.List
import           Data.Maybe
import           Control.Arrow
import           Test.QuickCheck

-- A pairing.
--
-- A pairing is a fixed-point-free involution \(f\) of
-- \(\{0, \ldots, 2(n + 1)\}\). Internally, it is represented by a list of
-- pairs of integers \(i, f(i)\), which we keep sorted, both on the level of
-- pairs and on the level of the list (lexicographically).
--
-- We disallow the empty pairing (a matter of convention).
newtype Pairing = Pairing' [(Int, Int)] deriving Eq
pattern Pairing
    :: [(Int, Int)]
    -- ^ List @ls@ of pairs which may not be sorted (throws exception if list
    --   is empty or if the pairing is invalid)
    -> Pairing
    -- ^ Pairing resulting from sorting the tuples of @ls@ then the list itself
pattern Pairing ls <- Pairing' ls where
    Pairing ls
        | null ls = error "empty pairing is illegal"
        | sort (uncurry (++) (unzip ls)) /= [0 .. length ls * 2 - 1] = error
            "invalid pairing"
        | otherwise = Pairing' (sortOn fst (map sortPair ls))

instance Show Pairing where
    show (Pairing ls) =
           "\\{"
        ++ intercalate ", " (map show ls)
        ++ "\\}"

instance Arbitrary Pairing where
    arbitrary = do
        -- To avoid combinatorial explosions, we use a relatively small domain.
        -- Alternatively, one could write an efficient ad hoc generation of
        -- permutations.
        n <- chooseInt (1, 7)
        elements (allPairings n)
    -- We shrink by taking faces.
    shrink p | numPairs p > 1 = [face i p | i <- [0..numPairs p - 1]]
             | otherwise      = []

-- | The number of pairs.
numPairs :: Pairing -> Int
numPairs (Pairing ls) = length ls
-- | The members/domain of a pairing.
members :: Pairing -> [Int]
members p = [0 .. maxValue p]
-- | The maximal value among the members.
maxValue :: Pairing -> Int
maxValue p = 2 * numPairs p - 1

-- | Sorts a pair.
sortPair :: (Int, Int) -> (Int, Int)
sortPair (a, b) | a <= b    = (a, b)
                | otherwise = (b, a)
-- | Decrements integer if it is above a threshold.
--
-- This is useful for getting rid of "holes" left by removing an integer:
--
-- >>> map (lower 4) ([1..3] ++ [5..10])
-- [1, 2, 3, 4, 5, 6, 7, 8, 9]
lower
    :: Int
    -- ^ Threshold \(n\)
    -> Int
    -- ^ Integer to be lowered \(m\) (throws exception if \(n = m\))
    -> Int
    -- ^ \(m - 1\) if \(n < m\) and \(m\) otherwise
lower n m | n < m     = m - 1
          | n > m     = m
          | otherwise = error "encountered removed level"
-- | Increments an integer if it is above a threshold.
--
-- This is useful for vacating integers from a list:
--
-- >>> map (vacate 2) [1..10]
-- [1, 3, 4, 5, 6, 7, 8, 9, 10, 11]
vacate
    :: Int
    -- ^ Threshold \(n\) (integer to be vacated)
    -> Int
    -- ^ Integer to be increased \(m\)
    -> Int
    -- ^ \(m + 1\) if \(n \leq m\) and \(m\) otherwise
vacate n m | n <= m    = m + 1
           | otherwise = m


-- | Deletes an index from a list.
deleteAt
    :: Int
    -- ^ Index @i@ (throws exception if out of bound)
    -> [a]
    -- ^ List @ls@
    -> [a]
    -- ^ @ls@ with index @i@ deleted
deleteAt i ls | i < length ls = take i ls ++ drop (1 + i) ls
              | otherwise     = error "index out of bound"

-- | Applies the involution corresponding to a pairing.
friend
    :: Int
    -- ^ Integer \(i\)
    -> Pairing
    -- ^ Pairing with involution \(f : \{0, \ldots, 2n\} \to \{0, \ldots, 2n\}\)
    --   (throws exception if \(i\) is not in domain)
    -> Int
    -- ^ Integer \(f(i)\)
friend n (Pairing ls) = if x == n then y else x
  where (x, y) = fromJust $ find (\(x, y) -> x == n || y == n) ls
-- | Tests that pairings are indeed involutions.
_prop_Involution p = do
    n <- chooseInt (0, maxValue p)
    return $ friend (friend n p) p == n

-- | Removes a pair from a pairing.
--
-- The pairings of various sizes assemble into a semisimplicial set with
-- @face i@ as \(d_i\).
face
    :: Int
    -- ^ Index \(i\) (throws exception if out of bound)
    -> Pairing
    -- ^ Pairing \(p\)
    -> Pairing
    -- ^ Pairing \(p'\) obtained from \(p\) by removing the \(i\)'th pair (and
    --   decrementing relevant integers to attain contagiosity)
face i (Pairing ls) = Pairing
    $ map ((lower x . lower y) *** (lower x . lower y)) (deleteAt i ls)
  where (x, y) = ls !! i
-- | Tests the simplicial identities.
_prop_SimplicialIdentities p = numPairs p >= 3 ==>
    do
        i <- chooseInt (0, numPairs p - 2)
        j <- chooseInt (i + 1, numPairs p - 1)
        return $ face i (face j p) == face (j - 1) (face i p)

-- | Inserts a pair into a pairing.
insertPair
    :: (Int, Int)
    -- ^ Pair \((n, m)\) to be inserted
    -> Pairing
    -- ^ Pairing \(p\)
    -> Pairing
    -- ^ Pairing which has \(p\) as a face and contains the pair \((n, m)\)
insertPair pair (Pairing ls) = Pairing
    -- Since @x <= y@ and we want to vacate both @x@ and @y@, it is important
    -- that we first vacate @x@ then @y@.
    ((x, y) : map ((vacate y . vacate x) *** (vacate y . vacate x)) ls)
  where (x, y) = sortPair pair

-- All pairings with a certain number of pairs.
allPairings
    :: Int
    -- ^ Number of pairs \(n\)
    -> [Pairing]
    -- ^ All pairings with \(n\) pairs
allPairings 0 = []
allPairings 1 = [Pairing [(0, 1)]]
allPairings n =
    -- Generate a list candidates for the 0'th face (using recursion) and use
    -- the pairings obtained by inserting all possible pairs that could have
    -- been removed by the 0'th face map. Such pairs will of course have @0@ in
    -- their first coordinate.
    [ insertPair (0, i) p'
    | p' <- allPairings (n - 1)
    , i  <- [1 .. 2 * n - 1]
    ]
-- | Tests that the number of generated pairings match a certain combinatorial
--   formula.
_prop_Length = do
    n <- chooseInt (1, 7)
    -- Wahl, Proposition 4.1(b)
    return $ length (allPairings n) == product [n + 1 .. 2 * (n - 1) + 2] `div` 2^n
